// console.log("hello")
let trainer = {
    name: "Ash Ketchum",
    age: 10,
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    friends: {
        hoenn: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    talk: function() {
        console.log("Pikachu! I choose you!")
    }
}

console.log(trainer);

console.log("Result of dot notation:")
console.log(trainer.name)

console.log("Result of square bracket notation:")
console.log(trainer['pokemon']);

console.log("Result of talk method:")
trainer.talk()

// constructor
function Pokemon(name, level) {
    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.atttack = level;

    // Methods
    this.tackle = function(target) {
        // let targetPokemonhealth;
        // targetPokemonhealth - this.atttack 

        console.log(this.name + " tackled " + target.name);
        target.health -= this.atttack;
        console.log(target.name + " health is now reduced to " + target.health);

        if (target.health <= 0) {
            target.faint()
        }

    };
    this.faint = function() {

        console.log(this.name + " fainted.");
    }
}

let pikachu = new Pokemon("Pikachu", 12);
console.log(pikachu)
let Geodude = new Pokemon("Geodude", 8);
console.log(Geodude)
let Mewtwo = new Pokemon("Mewtwo", 100);
console.log(Mewtwo)

// using the tackle method from pikachu with rattata as its target
Geodude.tackle(pikachu);
console.log(pikachu)
Mewtwo.tackle(Geodude);
console.log(Geodude)